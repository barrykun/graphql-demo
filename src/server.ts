import { readFileSync } from "fs";
import { Auth } from "./auth";
import { resolvers } from "./resolvers";

const ApolloServerOptions = {
  typeDefs: readFileSync("./query.gql", "utf8") as any,
  resolvers: resolvers,
  playground: true,
  introspection: true,
  context(ctx: any) {
    return {
      ...ctx,
      isAuth: false
    };
  },
  schemaDirectives: {
    auth: Auth
  }
};

export default ApolloServerOptions;
