import { fetch } from "apollo-server-env";
import { Datastore } from "@google-cloud/datastore";

const TodoEntity = "Todo";
const UserEntity = "TodoAppUser";
const datastore = new Datastore();

export interface Todo {
  name: string;
  done: boolean;
  type: "checklist" | "reminder";
  user: string;
}

export interface User {
  id: string;
  name: string;
  todos?: Todo[];
}

async function createNewTodo(todo: Todo): Promise<Todo> {
  console.log("createNewTodo", todo);

  // Return todo if it exists
  const exists = await getTodoByName(todo.name);
  if (exists) {
    return exists;
  }

  if (todo.user) {
    const userexists = await getUserById(todo.user);
    console.log(userexists);
  }

  await (datastore.insert as any)({
    key: datastore.key([TodoEntity]),
    data: [
      { name: "name", value: todo.name },
      { name: "done", value: todo.done },
      { name: "type", value: todo.type },
      { name: "user", value: todo.user ? datastore.key([UserEntity, Number(todo.user)]) : null }
    ]
  });

  // Query the new todo
  return getTodoByName(todo.name);
}

async function createNewUser(user: Partial<User>): Promise<User> {
  console.log("createNewUser", user);
  // Return todo if it exists
  const [exists] = await getUsers({ name: user.name });
  if (exists) {
    return exists;
  }

  const [result] = await (datastore.insert as any)({
    key: datastore.key([UserEntity]),
    data: [{ name: "name", value: user.name }]
  });

  const [new_user] = await datastore.get(result.mutationResults[0].key.path[0]);

  return new_user ? (new_user as any) : null;
}

async function getTodoByName(name: string): Promise<Todo> {
  const records = await getTodos({ name });
  return records[0] ? records[0] : null;
}

async function getTodos(filters?: Record<string, string>): Promise<Todo[]> {
  let query = await datastore.createQuery(TodoEntity);

  if (filters) {
    Object.keys(filters).forEach(key => {
      if (key === "user") {
        query = query.filter(key, "=", datastore.key([UserEntity, Number(filters[key])]));
      } else {
        query = query.filter(key, "=", filters[key]);
      }
    });
  }

  let [records] = await query.run();

  records = records.map(todo => ({
    name: todo.name,
    type: todo.type,
    done: todo.done,
    user: todo.user ? todo.user.id : null
  }));

  return records;
}

async function getUsers(filters?: Record<string, string>): Promise<User[]> {
  let query = await datastore.createQuery(UserEntity);

  if (filters) {
    Object.keys(filters).forEach(key => {
      query = query.filter(key, "=", filters[key]);
    });
  }

  let [records] = await query.run();

  records = records.map(user => ({
    id: (user as any)[datastore.KEY].id,
    name: user.name
  }));

  return records;
}

async function getUserById(id: string): Promise<User> {
  const [record] = await datastore.get(datastore.key([UserEntity, Number(id)]));

  return record
    ? {
        id: (record as any)[datastore.KEY].id,
        name: record.name
      }
    : null;
}

export const resolvers = {
  Query: {
    todos(_: any, { input }: any) {
      return getTodos(input);
    },
    async users() {
      return getUsers();
    },
    async reddit_posts() {
      const posts: any = await fetch("https://reddit.com/.json").then(res => res.json());
      return posts.data.children.map((child: any) => child.data);
    }
  },
  Mutation: {
    async new_todo(_: any, { input }: any) {
      // console.log("new_todo", input);
      return await createNewTodo(input);
    },
    async new_user(_: any, { input }: any) {
      // console.log("new_user", input);
      return await createNewUser(input);
    }
  },
  Todo: {
    async user(todo: Todo): Promise<User> {
      // console.log("Resolved Todo.user", todo);
      if (!todo.user) return null;

      const user = await getUserById(todo.user);

      return user;
    }
  },
  User: {
    async todos(user: User): Promise<Todo[]> {
      // console.log("user todos", user);
      return getTodos({ user: user.id });
    }
  }
};
