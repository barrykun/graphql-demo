import { SchemaDirectiveVisitor, AuthenticationError } from "apollo-server";
import { defaultFieldResolver } from "graphql";

export class Auth extends SchemaDirectiveVisitor {
  visitFieldDefinition(field: any) {
    const resolve = field.resolve || defaultFieldResolver;

    field.resolve = function(rootValue: any, args: any, context: any, info: any) {
      if (!context.isAuth) {
        throw new AuthenticationError("Not auth");
      }

      return resolve.call(this, rootValue, args, context, info);
    };
  }
}
