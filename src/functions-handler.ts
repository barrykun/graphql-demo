import { ApolloServer } from "apollo-server-cloud-functions";
import options from "./server";

const server = new ApolloServer(options);

exports.graphql = server.createHandler();
