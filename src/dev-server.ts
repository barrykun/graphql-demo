process.env.GOOGLE_APPLICATION_CREDENTIALS =
  "/Users/barthuijgen/.gcloud/private-project-credentials.json";

import { ApolloServer } from "apollo-server";
import options from "./server";

const server = new ApolloServer(options);

server
  .listen()
  .then(({ url }) => console.log(`Server running on ${url}`))
  .catch(console.error);
